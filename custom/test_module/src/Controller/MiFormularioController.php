<?php
namespace Drupal\test_module\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class MiFormularioController extends ControllerBase {
    public function formularioData() {
        $query = \Drupal::database()->select('test_module_data', 't');
        $query->fields('t');
        $results = $query->execute()->fetchAll();

        $output = '<table>';
        $output .= '<thead><tr><th>Apellido</th><th>Nombre</th><th>Tipo de documento</th><th>Número de documento</th><th>Correo electrónico</th><th>Teléfono</th><th>País</th></tr></thead>';
        $output .= '<tbody>';
        foreach ($results as $result) {
            $output .= '<tr>';
            $output .= '<td>' . $result->apellido . '</td>';
            $output .= '<td>' . $result->nombre . '</td>';
            $output .= '<td>' . $result->tipo_documento . '</td>';
            $output .= '<td>' . $result->numero_documento . '</td>';
            $output .= '<td>' . $result->correo_electronico . '</td>';
            $output .= '<td>' . $result->telefono . '</td>';
            $output .= '<td>' . $result->pais . '</td>';
            $output .= '</tr>';
        }
        $output.= '</tbody>';
        $output.= '</table>';
        return new Response($output);
    }
}
?>
