<?php
    namespace Drupal\test_module\Form;
    use Drupal\Core\Form\FormBase;
    use Drupal\Core\Form\FormStateInterface;

    class MiFormularioForm extends FormBase {
        /**
         * {@inheritdoc}
         */
        public function getFormId() {
            return 'mi_formulario';
        }
        /**
         * {@inheritdoc}
         */
        public function buildForm(array $form, FormStateInterface $form_state)
        {
            //agregamos los campo al formulario
            $form['apellido'] = array(
                '#type' => 'textfield',
                '#title' => t('Apellido'),
                '#required' => TRUE,
            );
            $form['nombre'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Nombre'),
                '#required' => TRUE,
            );
            $form['tipo_documento'] = array(
                '#type' => 'select',
                '#title' => $this->t('Tipo de Documento'),
                '#options' => array(
                    'TI' => $this->t('Tarjeta de Identidad'),
                    'CC' => $this->t('Cedula de Ciudadania'),
                    'CE' => $this->t('Cedula de Extranjeria'),
                ),
                '#required' => TRUE,
            );
            $form['numero_documento'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Numero de Documento'),
                '#required' => TRUE,
            );
            $form['correo_electronico'] = array(
                '#type' => 'email',
                '#title' => $this->t('Correo Electronico'),
                '#required' => TRUE,
            );
            $form['telefono'] = array(
                '#type' => 'tel',
                '#title' => $this->t('Telefono'),
                '#required' => TRUE,
            );
            $form['pais'] = array(
                '#type' => 'select',
                '#title' => $this->t('Pais'),
                '#options' => array(
                    'CO' => $this->t('Colombia'),
                    'PE' => $this->t('Peru'),
                    'EC' => $this->t('Ecuador'),
                ),
                '#required' => TRUE,
            );
            $form['submit'] = array(
                '#type' => 'submit',
                '#value' => t('Enviar')
            );
            return $form;
        }
        /**
         * {@inheritdoc}
         */
        public function validateForm(array &$form, FormStateInterface $form_state)
        {
            //validamos el correo electronico que sea valido
            if (!\Drupal::service('email.validator')->isValid($form_state->getValue('correo_electronico'))) {
                $form_state->setErrorByName('correo_electronico', $this->t('El correo electronico no es valido'));
            }
        }
        /**
         * {@inheritdoc}
         */
        public function submitForm(array &$form, FormStateInterface $form_state)
        {
            // Obtenemos los valores de los campos del formulario.
            $apellido = $form_state->getValue('apellido');
            $nombre = $form_state->getValue('nombre');
            $tipo_documento = $form_state->getValue('tipo_documento');
            $numero_documento = $form_state->getValue('numero_documento');
            $correo_electronico = $form_state->getValue('correo_electronico');
            $telefono = $form_state->getValue('telefono');
            $pais = $form_state->getValue('pais');
            // Creamos un array con los datos del formulario.
            $datos = array(
                'apellido' => $apellido,
                'nombre' => $nombre,
                'tipo_documento' => $tipo_documento,
                'numero_documento' => $numero_documento,
                'correo_electronico' => $correo_electronico,
                'telefono' => $telefono,
                'pais' => $pais
            );
            // Almacena los datos en la base de datos.
            \Drupal::database()->insert('test_module_data')
                ->fields($datos)
                ->execute();
            // Redirige al usuario a la página donde esta listado.
            $form_state->setRedirect('test_module.formulario_data');
        }
    }
