### Ilumno Drupal
![](https://www.ilumno.com/assets/icons/apple-icon-72x72.png)
1.	Crear un módulo personalizado con nombre “test_module”
2.	Crear un formulario dentro del modulo creado con los siguientes campos:
	- Apellido
	- Nombre
	- Tipo de Documento
	- Numero de documento
	- Correo electrónico
	- Teléfono
	- País
3.	El formulario creado debe verse en la URL “{site_url}/test/formulario”
4.	El formulario deberá validar que todos los campos sean obligatorios, y deben ser diligenciados en su totalidad
5.	El campo Correo electrónico debe validarse como un correo válido
6.	El campo Tipo de Documento y País, deben ser de tipo lista, y los valores deben ser traídos desde el vocabulario Tipo Documento y País respectivamente (Los vocabularios y términos deben crearse al momento de la instalación del módulo).
7.	Los datos del formulario deben ser almacenados.
8.	Se debe mostrar un listado de los datos almacenados en la ruta “{site_url}/test/formulario/data”

# Instalador
`module-drupal.zip`

![](https://icons.iconarchive.com/icons/sicons/basic-round-social/512/drupal-icon.png)